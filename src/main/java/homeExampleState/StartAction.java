package homeExampleState;


import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

public class StartAction extends State{
    private final Map<String, State> mainStates;
    private State selectedState;

    {
        Map<String, State> temp = new LinkedHashMap<>();
        temp.put("Show all items in work", new ShowAllItemsInWork(action));
        temp.put("Show all titles", new ShowTitles(action));
        mainStates = Collections.unmodifiableMap(temp);
    }

    public StartAction(Action action) {
        super(action);
    }

    public void setMainState(String usersChoice) {
        this.selectedState = mainStates.get(usersChoice);
    }



    @Override
    public void doing() {
        //show main menu
        nextAction();
    }

    @Override
    public void nextAction() {
        if(selectedState == null) {
            throw new NullPointerException();
        }
        action.changeState(selectedState);
    }

    @Override
    public void prevAction() {
        action.changeState(new StartAction(action));
    }
}
