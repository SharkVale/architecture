package homeExampleState;

public class ShowAllItems extends State{


    public ShowAllItems(Action action) {
        super(action);
    }

    @Override
    public void doing() {
        //vat listItems = findAll()
        nextAction();
    }

    @Override
    public void nextAction() {
        action.changeState(new ShowItemsInfo(action));
    }

    @Override
    public void prevAction() {
        action.changeState(new ShowTitles(action));
    }
}
