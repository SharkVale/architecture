package homeExampleState;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

public class Application {
    private final Map<String, State> mainStates;
    private State selectedState;

    {
        Map<String, State> temp = new LinkedHashMap<>();
        temp.put("Show all items in work", new ShowAllItemsInWork(action));
        temp.put("Show all titles", new ShowTitles(action));
        mainStates = Collections.unmodifiableMap(temp);
    }


    public static void main(String[] args) {
        Action userAction = new Action(bot, itemsService, titleService, userService);
        while (true) {
            if(bot.getUpdate != null) {
                String userChoice = bot.getUpdate.getMessage();
                userAction.setUserChoiceToStartState(userChoice);
                userAction.getState().doing();
            }
        }
    }
}
