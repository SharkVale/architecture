package homeExampleState;

public class ShowItemsInfo  extends State{
    public ShowItemsInfo(Action action) {
        super(action);
    }

    @Override
    public void doing() {
        //show info
        nextAction();
    }

    @Override
    public void nextAction() {
        action.changeState(new StartAction(action));
    }

    @Override
    public void prevAction() {
        action.changeState(new ShowAllItems(action));
    }
}
