package homeExampleState;

public class ShowAllItemsInWork extends State{

    public ShowAllItemsInWork(Action action) {
        super(action);
    }

    @Override
    public void doing() {
        //var listItems = findAllByInWork();
        //bot.sendMessage(listItems);
        nextAction();
    }

    @Override
    public void nextAction() {
        action.changeState(new ShowItemsInfo(action));
    }

    @Override
    public void prevAction() {
        action.changeState(new StartAction(action));
    }
}