package homeExampleState;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

public class Action {
    Bot bot;
    ItemsService itemsService;
    TitleService titleService;
    UserService userService

    private State state;
    public void Action(Bot bot, ItemsService itemsService, TitleService titleService, UserService userService) {
        this.bot = bot;
        this.itemsService = itemsService;
        this.titleService = titleService;
        this.userService = userService;
        this.state = new StartAction(this);
    }
    
    public void setUserChoiceToStartState(String userChoice) {
        if(state.getClass() == StartAction.class) {
            ((StartAction) state).setMainState(userChoice);
        } else if (userChoice.equals("prev")) {
            this.changeState(new GoBack(this));
        }
    }

    protected void changeState(State state) {
        this.state = state;
    }

    public State getState() {
        return state;
    }
}