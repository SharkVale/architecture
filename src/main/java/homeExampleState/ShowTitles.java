package homeExampleState;

public class ShowTitles extends State {
    public ShowTitles(Action action) {
        super(action);
    }

    @Override
    public void doing() {
        //var titlesList = action.titlesService.findAll();
        //bot.sendMessage(titlesList);
        nextAction();
    }

    @Override
    public void nextAction() {
        action.changeState(new ShowAllItems(action));
    }

    @Override
    public void prevAction() {
        action.changeState(new StartAction(action));
    }
}
