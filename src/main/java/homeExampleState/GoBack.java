package homeExampleState;

public class GoBack extends State{
    public GoBack(Action action) {
        super(action);
    }

    @Override
    public void doing() {
        //два раза так как уже установлено следующее действие
        action.getState().prevAction();
        action.getState().prevAction();
    }

    @Override
    public void nextAction() {

    }

    @Override
    public void prevAction() {

    }
}
