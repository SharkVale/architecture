package homeExampleState;


public abstract class State {
    Action action;

    protected State(Action action) {
        this.action = action;
    }

    protected abstract void doing();
    protected abstract void nextAction();
    protected abstract void prevAction();

}