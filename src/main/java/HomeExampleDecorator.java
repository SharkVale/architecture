//package com.example.storenotificationbot;
//
//import com.example.storenotificationbot.dao.UsersDao;
//import com.example.storenotificationbot.service.UserService;
//import org.telegram.telegrambots.meta.generics.TelegramBot;
//
//import java.util.List;
//
//public class HomeExampleDecorator {
//    public interface Notifier {
//        void notifyUsers(String meesage);
//        List<UsersDao> getUsers();
//    }
//
//    public class TelegramUsersNotifier implements Notifier{
//        private final UserService userService;
//        private final List<UsersDao> usersDaoList;
//        public TelegramUsersNotifier(UserService userService, Bot bot) {
//            this.userService = userService;
//            usersDaoList = userService.findAll();
//        }
//
//        @Override
//        public void notifyUsers(String message) {
//            for (UsersDao user : usersDaoList) {
//                bot.sendTextMessage(user);
//            }
//        }
//
//
//        public List<UsersDao> getUsers() {
//            return usersDaoList;
//        }
//    }
//
//
//
//    public class BaseTelegramUsersNotifierDecorator implements Notifier {
//        protected Notifier wrapper;
//
//        public BaseTelegramUsersNotifierDecorator(Notifier notifier) {
//            this.wrapper = wrapper;
//        }
//
//        @Override
//        public void notifyUsers(String meesage) {
//            wrapper.notifyUsers(meesage);
//        }
//
//        @Override
//        public List<UsersDao> getUsers() {
//            return wrapper.getUsers();
//        }
//    }
//
//
//    public class EmailUsersNotifierDecorator extends BaseTelegramUsersNotifierDecorator {
//
//        public EmailUsersNotifierDecorator(Notifier notifier) {
//            super(notifier);
//        }
//
//        @Override
//        public void notifyUsers(String meesage) {
//            List<UsersDao> usersDaoList = super.getUsers();
//            for (UsersDao usersDao : usersDaoList) {
//                //send email
//            }
//            wrapper.notifyUsers(meesage);
//        }
//    }
//
//
//
//    public class VKUsersNotifierDecorator extends BaseTelegramUsersNotifierDecorator {
//
//        public VKUsersNotifierDecorator(Notifier notifier) {
//            super(notifier);
//        }
//
//        @Override
//        public void notifyUsers(String meesage) {
//            List<UsersDao> usersDaoList = super.getUsers();
//            for (UsersDao usersDao : usersDaoList) {
//                //send VK message
//            }
//            wrapper.notifyUsers(meesage);
//        }
//    }
//
//
//
//    public class App {
//        private UserService userService;
//        private Bot bot;
//
//        public void main(String[] args) {
//            Notifier baseNotifier = new TelegramUsersNotifier(userService, bot);
//            Notifier notifierAnywhere = new BaseTelegramUsersNotifierDecorator();
//            notifierAnywhere = new EmailUsersNotifierDecorator(new VKUsersNotifierDecorator(notifierAnywhere));
//            notifierAnywhere.notifyUsers("A message for everyone!!!");
//        }
//    }
//
//}
