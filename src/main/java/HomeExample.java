//package com.example.storenotificationbot;
//
//import com.example.storenotificationbot.actions.Action;
//
//import java.util.Map;
//
//public class HomeExample {
//
//    public abstract class AbstractActionFactory {
//        abstract Action getAction();
//    }
//
//    public class AddItemActionFactory extends AbstractActionFactory{
//        @Override
//        Action getAction() {
//            return new AddItem();
//        }
//    }
//
//    public class AddTitleFactory extends AbstractActionFactory{
//        @Override
//        Action getAction() {
//            return new AddTitle();
//        }
//    }
//
//
//    public abstract class Action {
//        abstract void execute();
//    }
//
//    public class AddItem extends Action {
//        @Override
//        public void execute() {
//            //add item logic
//        }
//    }
//
//    public class AddTitle extends Action {
//        @Override
//        public void execute() {
//            //add title logic
//        }
//    }
//
//
//
//
//  public class Application {
//        final static Map<String, Class> fabricMap = Map.of("/addItem", AddItemActionFactory.class,
//                "/addTitle", AddTitleFactory.class);
//
//      public static void main(String[] args) {
//          AbstractActionFactory factory = fabricMap.get(args[0]).getDeclaredConstructor().newInstance();
//          Action action = factory.getAction();
//          action.execute();
//      }
//  }
//
//}
